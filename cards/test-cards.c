#include "card.h"
#include "cards.h"

#include <locale.h>

/* Antes de compilar, verifique se a biblioteca libglib2.0-dev e a ferramenta p>
 * Se não:
 * $ sudo apt install pkg-config libglib2.0-dev
 * Compilando códigos com GLib:
 * $ gcc -c [arquivos].c `pkg-config glib-2.0 --cflags
 * Gerando executável:
 * $ gcc [arquivos].o `pkg-config glib-2.0 --libs` -o executavel.run
 */

void print_each(void * pdata, void * pindex) {
  BCard * card = (BCard *) pdata;
  int * idx = (int *) pindex;
  char repr[B_CARD_REPR_SIZE];
  b_card_decode(repr, *card);
  g_print("Carta %d: %s\n", (*idx)++, repr);
}

int main() {
  BCard * card;
  GList * list = NULL;

  setlocale(LC_ALL, "pt_BR.utf-8");

  g_print(" * Adicionando cartas...\n");
  card = g_new(BCard, 1);
  b_card_code(card, "2♥");
  list = g_list_append(list, card);
  card = g_new(BCard, 1);
  b_card_code(card, "A♦");
  list = g_list_append(list, card);
  card = g_new(BCard, 1);
  b_card_code(card, "Q♠");
  list = g_list_append(list, card);

  int idx = 0;
  g_print(" * Cartas adicionadas:\n");
  g_list_foreach(list, print_each, &idx);
  g_list_free_full(list, g_free);
  return 0;
}
