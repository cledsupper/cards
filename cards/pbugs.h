/* pbugs.h - funções para tratamento de bugs em parâmetros de funções. */

#include <stdio.h>

/* Pode-se definir a diretiva NDEBUG no compilador para remover os tratamentos de erros, então otimizandoo o programa.
 * NOTE: isto só é recomendado para versões massivamente testadas.
 * Exemplo:
 * $ gcc codigo-usando-pbugs.c -DNDEBUG -o executavel */
#ifdef NDEBUG

#define ABORT_IF_NULL(ARG_N, ARG) 

#define ABORT_IF(ARG_N, ARG, IF_EXPR) 

#else

#define ABORT_IF_NULL(ARG_N, ARG) {\
  if (!(ARG)) {\
    fprintf(stderr, "EM %s(): parâmetro %d com valor nulo!\n",\
      __func__, ARG_N);\
    abort();\
  }\
}

#define ABORT_IF(ARG_N, ARG, IF_EXPR) {\
  if (IF_EXPR) {\
    fprintf(stderr, "EM %s(): parâmetro %d: %s!\n",\
      __func__, ARG_N, #IF_EXPR);\
    abort();\
  }\
}

#endif