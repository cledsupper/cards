/* cards.h - funções úteis para ler listas de cartas a partir de strings */

#ifndef _B_CARDS_H_
#define _B_CARDS_H_

/* Antes de compilar, verifique se a biblioteca libglib2.0-dev e a ferramenta pkg-config estão instalados.
 * Se não:
 * $ sudo apt install pkg-config libglib2.0-dev
 * Compilando códigos com GLib:
 * $ gcc -c [arquivos].c `pkg-config glib-2.0 --cflags
 * Gerando executável:
 * $ gcc [arquivos].o `pkg-config glib-2.0 --libs` -o executavel.run
 */
#include <glib.h>

#include "card.h"

/**
 * Lê toda as cartas da string e salva na lista.
 *
 * @param line string de leitura.
 * @param list lista de cartas.
 * @return a lista atuaalizada.
 */
GList * b_cards_code(GList * list, const char * line);

/**
 * Escreve toda a lista de cartas no final da string.
 *
 * @param line string para escrever as cartas.
 * @param size tamanho da string.
 * @param list lista de cartas.
 */
void b_cards_decode(char * line, size_t size, const GList * list);

/**
 * Compara duas cartas.
 * 
 * @param card_a
 * @param card_b
 * @return se card_a > card_b: 1; card_a = card_b: 0; ou então -1
 */
int b_cards_compare(BCard card_a, BCard card_b);

#endif
