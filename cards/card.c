/* card.c - Estrutura de dados para uma carta de baralho. */

#include "card.h"
#include "pbugs.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

static const int NUM_REPR_N = 13;
static const char * NUM_REPR[] = {
  "A",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "J",
  "Q",
  "K"
};

static const int SUIT_REPR_N = 4;
static const char * SUIT_REPR[] = {
    "♠",
    "♣",
    "♥",
    "♦"
};

const char * b_card_code(BCard * card, const char * repr) {
  ABORT_IF_NULL(1, card)
  ABORT_IF_NULL(2, repr)

  if (!(*repr)) return NULL;

  // avança caracteres inúteis
  //puts(" * b_card_code(): codificando número...");
  while (*repr) {
    if (isalnum(*repr)) break;
    repr++;
  }
  if (!(*repr)) return NULL;

  // codificar número
  int num;
  switch (*repr) {
    case 'A':
      card->num = BCARD_NUM_A;
      break;
    case 'J':
      card->num = BCARD_NUM_J;
      break;
    case 'Q':
      card->num = BCARD_NUM_Q;
      break;
    case 'K':
      card->num = BCARD_NUM_K;
      break;
    default:
      sscanf(repr, "%d", &num);
      // Representação de '2' é 1
      card->num = num-1;
      //printf("... num = %d\n", num);
  }
  repr++;
  if (card->num == BCARD_NUM_10)
    repr++;

  //puts(" * b_card_code(): codificando naipe...");
  num = strlen(SUIT_REPR[0]);
  if (!strncmp(repr, SUIT_REPR[BCARD_SUIT_SPADES], num))
    card->suit = BCARD_SUIT_SPADES;
  if (!strncmp(repr, SUIT_REPR[BCARD_SUIT_CLUBS], num))
    card->suit = BCARD_SUIT_CLUBS;
  if (!strncmp(repr, SUIT_REPR[BCARD_SUIT_HEARTS], num))
    card->suit = BCARD_SUIT_HEARTS;
  if (!strncmp(repr, SUIT_REPR[BCARD_SUIT_DIAMONDS], num))
    card->suit = BCARD_SUIT_DIAMONDS;

  //printf("... naipe = %3s\n", repr);
  repr += num;

  //puts(" * b_card_code(): FIM");
  return repr;
}

void b_card_decode(char * repr, BCard card) {
  ABORT_IF_NULL(1, repr)

  //printf(" * b_card_decode(): escrevendo número... (%d)\n", card.num);
  strcpy(repr, NUM_REPR[card.num]);
  //puts(" * b_card_decode(): escrevendo naipe...");
  strcat(repr, SUIT_REPR[card.suit]);
}
