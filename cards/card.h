/* card.h - Estrutura de dados para uma carta de baralho. */

#ifndef _B_CARD_H
#define _B_CARD_H

typedef enum {
  BCARD_NUM_A, // = 0
  BCARD_NUM_2, // = 1
  BCARD_NUM_3,
  BCARD_NUM_4,
  BCARD_NUM_5,
  BCARD_NUM_6,
  BCARD_NUM_7,
  BCARD_NUM_8,
  BCARD_NUM_9,
  BCARD_NUM_10,
  BCARD_NUM_J,
  BCARD_NUM_Q,
  BCARD_NUM_K,
} BCardNum;

typedef enum {
  BCARD_SUIT_SPADES,
  BCARD_SUIT_CLUBS,
  BCARD_SUIT_HEARTS,
  BCARD_SUIT_DIAMONDS
} BCardSuit;

/** b_card - Estrutura para representar a carta do jogador. */
typedef struct b_card {
  BCardNum   num;
  BCardSuit  suit;
} BCard;

/** Tamanho necessário para uma string representar uma carta */
static const int B_CARD_REPR_SIZE = 6;

/**
 * Codifica a representação em string UTF-8 da carta em uma BCarta.
 *
 * @param card endereço de um objeto BCard.
 * @param repr string representado a carta. Exemplo: 2♠
 * @return posição final da string
 */
const char * b_card_code(BCard * card, const char * repr);

/**
 * Decodifica uma BCard em texto UTF-8.
 *
 * @param card
 * @param repr uma string que deve possuir espaço para seis caracteres (inclui nulo).
 */
void b_card_decode(char * repr, BCard card);

#endif
