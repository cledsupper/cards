/* cards.c - funções úteis para ler listas de cartas a partir de strings */

#include "cards.h"

GList * b_cards_code(GList * list, const char * line) {
  BCard * card_new = g_new(BCard, 1);
  const char * pline = line;
  while ((pline = b_card_code(card_new, pline))) {
    list = g_list_append(list, card_new);
    card_new = g_new(BCard, 1);
  }
  g_free(card_new);
  return list;
}

void b_cards_decode(char * line, size_t size, const GList * list) {
  const GList * it = list;
  char repr[B_CARD_REPR_SIZE];
  do {
    BCard * card = (BCard *) it->data;
    b_card_decode(repr, *card);
    g_strlcat(line, repr, size);
    g_strlcat(line, " ", size);
    it = it->next;
  } while (it);
  line[strlen(line)-1] = '\0';
}

int b_cards_compare(BCard a, BCard b) {
  if (a.suit == b.suit)
    return a.num - b.num;
  return a.suit - b.suit;
}
